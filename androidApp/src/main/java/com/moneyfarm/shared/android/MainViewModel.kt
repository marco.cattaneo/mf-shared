package com.moneyfarm.shared.android

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.moneyfarm.shared.core.data.FeedRepository
import kotlinx.coroutines.launch
import java.lang.Exception

class MainViewModel constructor(
    private val feedRepository: FeedRepository
): ViewModel() {
    fun fetch() {
        viewModelScope.launch {
            try {
                val res = feedRepository.fetchFeed(
                    url = "https://blog.moneyfarm.com/it/tag/mobile/?feed=mobile"
                )

                res.desc
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

}
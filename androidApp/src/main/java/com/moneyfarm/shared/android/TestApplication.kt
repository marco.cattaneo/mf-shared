package com.moneyfarm.shared.android

import android.app.Application
import com.moneyfarm.shared.core.data.FeedRepository
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.module

class TestApplication: Application() {

    override fun onCreate() {
        super.onCreate()

        val module = module {
            factory { FeedRepository() }
            viewModel { MainViewModel(feedRepository = get()) }
        }

        startKoin {
            modules(module)
        }
    }

}
package com.moneyfarm.shared.core.data

import com.moneyfarm.shared.core.FeedParser
import com.moneyfarm.shared.core.domain.Feed
import com.moneyfarm.shared.core.network.FeedHttpClient
import com.moneyfarm.shared.core.network.HttpClientSpecs
import io.ktor.client.*
import io.ktor.client.request.*

class FeedRepository {

    private val httpClient: HttpClient = FeedHttpClient(engine = HttpClientSpecs().engine)
    private val feedParser: FeedParser = FeedParser()

    @Throws(Exception::class)
    suspend fun fetchFeed(
        url: String
    ): Feed {
        val xml: String = httpClient.get(url)
        return feedParser.parse(
            sourceUrl = url,
            xml = xml,
            isDefault = false
        )
    }

}
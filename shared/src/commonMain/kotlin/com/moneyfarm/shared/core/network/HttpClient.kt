package com.moneyfarm.shared.core.network

import io.ktor.client.*
import io.ktor.client.engine.*

expect class HttpClientSpecs() {
    val engine: HttpClientEngine
}

fun FeedHttpClient(
    withLog: Boolean = false,
    engine: HttpClientEngine
): HttpClient = HttpClient(engine) {
    /*if (withLog) install(Logging) {
        level = LogLevel.HEADERS
        logger = object : Logger {
            override fun log(message: String) {
                Napier.v(tag = "IosHttpClient", message = message)
            }
        }
    }*/
}
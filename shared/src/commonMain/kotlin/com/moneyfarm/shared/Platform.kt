package com.moneyfarm.shared

expect class Platform() {
    val platform: String
}
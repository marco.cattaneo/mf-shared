package com.moneyfarm.shared.core.network

import io.ktor.client.engine.*
import io.ktor.client.engine.ios.*

actual class HttpClientSpecs actual constructor(){
    actual val engine: HttpClientEngine = Ios.create()
}
import SwiftUI
import shared

struct ContentView: View {
	let greet = Greeting().greeting()
    let feedRepository = FeedRepository()

    func fetch() {
        feedRepository.fetchFeed(url: "https://blog.moneyfarm.com/it/tag/mobile/?feed=mobile") {
            feed, error in
            
            print(feed)
        }
    }
    
    
    init() {
        fetch()
    }
    
	var body: some View {
		Text(greet)
	}
}

struct ContentView_Previews: PreviewProvider {
	static var previews: some View {
		ContentView()
	}
}
